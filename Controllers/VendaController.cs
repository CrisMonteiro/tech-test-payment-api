using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entitys;

namespace tech_test_payment_api.Controllers
{   
    [ApiController]
    [Route("[controller]")]

    public class VendaController :ControllerBase
    {
        private readonly VendaContext _context;
        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("Registrar Venda")]
        public IActionResult Create(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }
        [HttpGet("Buscar Venda")]
        public IActionResult BuscarVenda(int Id)
        {
            Venda venda = _context.Vendas.Find(Id);
            if(venda == null)
            {
                 return NotFound();
            }
           
            return Ok(venda);
        }
        

        [HttpPut("Atualizar Venda")]
        public ActionResult<List<Venda>> Atualizar (EnumStatusVenda Status)
        {
         List<Venda> vendas = _context.Vendas.Where(x => x.Status == Status).ToList();
          if(vendas == null)
          {
             return NotFound();
          }
            return Ok(vendas);
          
           
        }

        [HttpDelete("Deletar")]
        public IActionResult Deletar(int Id)
        {
            var registroBanco = _context.Vendas.Find(Id);
            if(registroBanco == null)
            return NotFound();

            _context.Vendas.Remove(registroBanco);
            _context.SaveChanges();

            return NoContent();

        }

    }
}